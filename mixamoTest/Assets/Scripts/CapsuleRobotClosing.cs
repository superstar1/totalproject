﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRobotClosing : MonoBehaviour
{
    // 주변을 둘러보며 감탄한다. "정말 아름답다"
    // 플레이어에게 고맙다고 인사한다. "고마워! 네 덕분에 ~"
    // 춤추는 친구 지켜봄
    // 친구가 말할 때 플레이어 봄
    // Start is called before the first frame update

    float currentTime;
    bool LegDelay3;
    GameObject cr, player;
    Animator crCAnim;


    public GameObject rbCManager;

    void Start()
    {
        cr = GameObject.Find("CR");
        //cr = GameObject.Find("BetaforCR");
        player = GameObject.Find("Player");
        crCAnim = cr.GetComponent<Animator>();
        crCAnim.runtimeAnimatorController = Resources.Load("crCloAnim") as RuntimeAnimatorController;
   
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        crCAnim.SetFloat("Time", currentTime);
        if (currentTime > 4.3f && currentTime < 20)
        {
            //this.transform.rotation = Quaternion.AngleAxis(15, this.transform.up);
            Vector3 dir2 = player.transform.position - this.transform.position;
            dir2 = Vector3.ProjectOnPlane(dir2.normalized, Vector3.up);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir2), 1.3f * Time.deltaTime);

        }

        if (currentTime > 9.5f && currentTime < 20)
        {
            crCAnim.SetBool("LegDelay3", true);
        }
        

    }
}
