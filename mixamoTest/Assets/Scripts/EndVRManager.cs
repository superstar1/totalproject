﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndVRManager : MonoBehaviour
{
    GameObject brPlate, crPlate;
    //Text brText, crText;
    bool brIsTalking, crIsTalking;

    bool stopped, pickedup, afterc1, aftersurprised, metplayer;

    float currentTime;

    TypeWriterEffect typeWriterEffectbr = null;
    TypeWriterEffect typeWriterEffectcr = null;
    // Start is called before the first frame update
    void Start()
    {
        //brText = GameObject.Find("brText").GetComponent<Text>(); //brPlate보다 아랫줄에 있으면 못찾음!
        brPlate = GameObject.Find("brPlate");
        typeWriterEffectbr = GameObject.Find("brText").GetComponent<TypeWriterEffect>();
        brPlate.SetActive(false);

        //crText = GameObject.Find("crText").GetComponent<Text>();
        crPlate = GameObject.Find("crPlate");
        typeWriterEffectcr = GameObject.Find("crText").GetComponent<TypeWriterEffect>();
        crPlate.SetActive(false);
    }

    public void brSays()
    {
        //print("br대사 나옴");
        /* if (brIsTalking == true)
         {
             brPlate.SetActive(true);
         }
         else
         {
             brPlate.SetActive(false);
         }*/


        if (1f < currentTime && currentTime < 3f)
        {
            brPlate.SetActive(true);
            //brIsTalking = true;
            typeWriterEffectbr.Get_Typing("우와!");
            //brText.text = "우와!";
        }
        else if (currentTime > 3 && currentTime < 7f)
        {
            brPlate.SetActive(false);
            // brIsTalking = false;
        }
        else if (6f < currentTime && currentTime < 10f) //7
        {
            brPlate.SetActive(true);
            //brIsTalking = true;
            typeWriterEffectbr.Get_Typing("고마워!네 덕분에 지구가 아름다운 자연을 되찾았어.");
            //brText.text = "고마워!네 덕분에 지구가 아름다운 자연을 되찾았어.";
        }
        //if (currentTime > 9 && currentTime < 12f)  //8 11
        //{
        //    brIsTalking = true;
        //    brText.text = "네 덕분에 지구가 아름다운 자연을 되찾았어.";
        //}
        else if (currentTime > 10.5f)
        {
            brPlate.SetActive(false);
        }

    }

    public void crSays()
    {

        //print("cr대사 나옴");
        /*if (crIsTalking == true)
        {
            crPlate.SetActive(true);
        }
        else
        {
            crPlate.SetActive(false);
        }
*/
        if (3f < currentTime && currentTime < 6f)
        {
            // crIsTalking = true;
            crPlate.SetActive(true);
            typeWriterEffectcr.Get_Typing("이게 지구의 원래 모습이구나.");
            //crText.text = "이게 지구의 원래 모습이구나.";
        }

        //if (6f < currentTime && currentTime < 7f)
        //{
        //    crIsTalking = true;
        //    crText.text = "신나~!";
        //}

        //if (5f < currentTime && currentTime < 7f)
        //{
        //    crIsTalking = true;
        //    crText.text = "지구는 원래 이런 모습이었구나.";
        //}
        //if (currentTime > 6 && currentTime < 11f) //7 7.3
        //{
        //    crIsTalking = false;
        //}
        //if (7f < currentTime && currentTime < 8f) // 7.3 9.5
        //{
        //    crIsTalking = true;
        //    crText.text = "신나~!";
        //}
        if (currentTime > 6 && currentTime < 10f) //9,5
        {
            crPlate.SetActive(false);
        }
        else if (10.5f < currentTime && currentTime < 16f)
        {
            crPlate.SetActive(true);
            typeWriterEffectcr.Get_Typing("네 도움이 필요한 친구들이 더 있어. 도와줄래?");
            //crText.text = "네 도움이 필요한 친구들이 더 있어. 도와줄래?";
        }
        else if (currentTime > 16)
        {
            crPlate.SetActive(false);
        }

    }


    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        print("시간: " + currentTime);
        brSays();
        crSays();
    }
}