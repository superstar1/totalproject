﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartVRManager : MonoBehaviour
{
    GameObject brPlate, crPlate;
    Text brText, crText;
    bool brIsTalking, crIsTalking;

    bool stopped, pickedup, afterc1, aftersurprised, metplayer;

    float currentTime;

    // Start is called before the first frame update
    void Start()
    {
        brText = GameObject.Find("brText").GetComponent<Text>(); //brPlate보다 아랫줄에 있으면 못찾음!
        brPlate = GameObject.Find("brPlate");
        brPlate.SetActive(false);

        crText = GameObject.Find("crText").GetComponent<Text>();
        crPlate = GameObject.Find("crPlate");
        crPlate.SetActive(false);
    }

    public  void  brSays()
    {
        //print("br대사 나옴");
        if (brIsTalking == true)
        {
            brPlate.SetActive(true);
        }
        else
        {
            brPlate.SetActive(false);
        }

       
        if (5f < currentTime && currentTime< 6f)
        {
            brIsTalking = true;
            brText.text = "어?  이게 뭐지?";
        }
        if(currentTime>6 && currentTime< 10f)
        {
            brIsTalking = false;
        }
        if (10f < currentTime && currentTime < 15f)
        {
            brIsTalking = true;
            brText.text = "사람들이 사용하던 걸까? 납작하고 패여 있는 걸 보니, 여기에 음식이라는 걸 담았나 봐.";
        }
        if (currentTime > 15 && currentTime < 24f)
        {
            brIsTalking = false;
        }
        if (24f < currentTime && currentTime < 26f)
        {
            brIsTalking = true;
            brText.text = "이게 도대체 뭘까?";
        }
        if (currentTime > 26)
        {
            brIsTalking = false;
        }

        //나레이션 후

        if (30f < currentTime && currentTime < 33f)
        {
            brIsTalking = true;
            brText.text = "안녕, 너 혹시 이게 뭔 줄 아니?";
        }
        if (currentTime > 33)
        {
            brIsTalking = false;
        }

    }
    public  void crSays()
    {

        //print("cr대사 나옴");
        if (crIsTalking == true)
        {
            crPlate.SetActive(true);
        }
        else
        {
            crPlate.SetActive(false);
        }

        if (7.5f < currentTime && currentTime < 9.5f)
        {
            crIsTalking = true;
            crText.text = "납작하고 지저분해.";
        }
        if (currentTime >9.5f  && currentTime < 15f)
        {
            crIsTalking = false;
        }
        if (15f < currentTime && currentTime < 17f)
        {
            crIsTalking = true;
            crText.text = "흠.......";
        }
        if (currentTime > 17 && currentTime < 20f)
        {
            crIsTalking = false;
        }
        if (20f < currentTime && currentTime < 23f)
        {
            crIsTalking = true;
            crText.text = "깜짝이야! 위험한 물건 아니야?";
        }
        if (currentTime > 23)
        {
            crIsTalking = false;
        }
    }


// Update is called once per frame
void Update()
    {
        currentTime += Time.deltaTime;
        brSays();
        crSays();
    }
}

// 와아!
// 정말 아름답다.
// 지구는 원래 이런 모습이었구나.
// 정말 고마워! 네 덕분에 건강한 지구의 모습을 볼 수 있게 되었어.
// 네 도움이 필요한 친구들이 더 있어. 도와줄래?
