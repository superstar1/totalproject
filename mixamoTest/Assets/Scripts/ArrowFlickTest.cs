﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowFlickTest : MonoBehaviour
{
    MeshRenderer meshRenderer = null;
    Color currentEmissionColor;
    public float intensityValue = 2;
    float currentTime = 0f;
    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        currentEmissionColor = meshRenderer.material.GetColor("_EmissionColor");
    }

    // Update is called once per frame
    void Update()
    {
        //currentTime += Time.deltaTime;
        meshRenderer.material.SetColor("_EmissionColor", currentEmissionColor * Mathf.Abs(Mathf.Sin(Time.time)) * intensityValue); 
        //주기 변화, -내려가지 않게 절댓값 , 시간 변화에 따라 값이 바뀌어야 하니까 Sin값에 Time.deltaTime; (시간 값 저장해야 돼서 float로 currentTime받아야함) 
    }
}
