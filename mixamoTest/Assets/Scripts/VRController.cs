﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRController : MonoBehaviour
{
    GameObject mc;
    public float walkSpeed = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        mc = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 input = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        Vector3 movement = mc.transform.TransformDirection(input.x, 0, input.y);
        movement.y = 0;
        movement *= Time.deltaTime * walkSpeed;
        this.transform.Translate(movement);
    }
}
