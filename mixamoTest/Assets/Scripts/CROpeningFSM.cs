﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CROpeningFSM : MonoBehaviour
{
    GameObject player, canPos, br, can, crPlate;
    Text crText;
    Animator crAnim;
    float currentTime, dist;
    Vector3 dir;
    TypeWriterEffect typeWriterEffect = null;

    enum crState
    {
        Idle, Walk, Wonder, Surprised
    }
    crState cr_State;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        canPos = GameObject.Find("CanPos1");
        //can = GameObject.Find("Can");
        br = GameObject.Find("BR");
        //can.SetActive(false);

        crAnim = GetComponent<Animator>();
        crAnim.runtimeAnimatorController = Resources.Load("crAnim") as RuntimeAnimatorController;


        crPlate = GameObject.Find("crPlate");
        typeWriterEffect = GameObject.Find("crText").GetComponent<TypeWriterEffect>();
       // crText = GameObject.Find("crText").GetComponent<Text>();
        crPlate.SetActive(false);

        cr_State = crState.Walk;
    }

    private void Idle()
    {

    }
    private void Walk()
    {
        //print("C상태: Walk");

        if (currentTime < 5)
        {
            dir = (canPos.transform.position - this.transform.position).normalized;
            dir = Vector3.ProjectOnPlane(dir, Vector3.up);
            transform.forward = dir;
            //if(currentTime<11)
        }
        else if(currentTime> 5)
        {
            cr_State = crState.Wonder;
        }
    }
    private void Wonder()
    {
        //print("C상태: Walk-> LiftandWonder");
        if (currentTime < 6) // || 16<currentTime)
        {
            crAnim.SetBool("LegDelay", false);
        }
        else if (6 < currentTime && currentTime < 17) //16
        {
            crAnim.SetBool("LegDelay", true);  // 생각하는 동안 angry 애니메이션에서 다리는 idle
            //if (10 < currentTime && currentTime < 13) 파란색 로봇 쪽 보기
            //{
            //    crAnim.SetBool("HeadOnly", true);
            //}
            //else if (13 < currentTime && currentTime< 15.5)
            //{
            //    crAnim.SetBool("HeadOnly", false);
            //}
            if (6.3f < currentTime && currentTime < 7.3f)
            {
                Vector3 dir2 = br.transform.position - this.transform.position;
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir2), 3f * Time.deltaTime);
                // this.transform.rotation = Quaternion.AngleAxis(25, this.transform.up);
            }
            else if (8.5f < currentTime && currentTime < 10.5f)
            {
                crPlate.SetActive(true);
                typeWriterEffect.Get_Typing("우리 몸처럼 단단해 보인다.");
                //crText.text = "우리 몸처럼 단단해 보인다.";
            }
            else if (currentTime > 10.5f && currentTime < 14f)
            {
                crPlate.SetActive(false);
            }

            else if(15< currentTime && currentTime < 17f)
            {
                crPlate.SetActive(true);
                typeWriterEffect.Get_Typing("무슨 로봇이 이렇게 작아?");
               // crText.text = "무슨 로봇이 이렇게 작아?";
            }
            else if (currentTime > 17)
            {
                crPlate.SetActive(false);
            }
        }
        else if (17.5f < currentTime) //16<
        {
            cr_State = crState.Surprised;
        }
    }
    private void Surprised()
    {
        //print("C상태: Wonder-> Surprised");
        crAnim.SetBool("LegDelay", false);

        if (17.5f < currentTime && currentTime < 18.5f) //<19.5
        {
            crPlate.SetActive(true);
            typeWriterEffect.Get_Typing("으악!");
            //crText.text = "으악!";
        }
        if (currentTime > 19.5f) // && currentTime < 23f)
        {
            crPlate.SetActive(false);
        }
       
    }

        // Update is called once per frame
        void Update()
        {
            currentTime += Time.deltaTime;
            //print("time: " + currentTime);
            crAnim.SetFloat("Time", currentTime);
            

            switch (cr_State)
            {
                case crState.Idle:
                    Idle();
                    break;

                case crState.Walk:
                    Walk();
                    break;

                case crState.Wonder:
                    Wonder();
                    break;

                case crState.Surprised:
                    Surprised();
                    break;
            }
        }
}