﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueRobot : MonoBehaviour
{
    // 쓰레기산 사이를 걸어간다
    // 쓰레기를 발견하고 줍는다
    // 마주보고 궁금해한다.
    // 나레이션이 끝나면
    // 플레이어(카메라)를 발견하고 다가온다.
    // Start is called before the first frame update

    float strollSpeed = 0.5f;
    float walkToPlayerSpeed = 1;
    float currentTime;
    float dist;

    //bool Stroll, StrollEnd, StopEnd, PickupEnd, LookEnd, WonderEnd, TalkEnd, SurpriseEnd;
    bool LegDelay, PlayerInRange;
    GameObject br, mc, can;
    Animator brAnim;

    public GameObject rbManager;

    Vector3 dir; //추가
   
    void Start()
    {
        br = GameObject.Find("AlphaforBR");
        mc = GameObject.Find("Main Camera");
        can = GameObject.Find("Can");
        brAnim = GetComponent<Animator>();
        brAnim.runtimeAnimatorController = Resources.Load("brAnim") as RuntimeAnimatorController;
        

    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        brAnim.SetFloat("Time", currentTime);

        // 0~5초 쓰레기더미 사이를 걷기
        if (currentTime< 5)
        {
           
                                     // this.transform.position += this.transform.forward * strollSpeed * Time.deltaTime;
        }

        if (11 < currentTime && currentTime < 16)
        {
            brAnim.SetBool("LegDelay", true);
        }
        else
        {
            brAnim.SetBool("LegDelay", false);
        }
        //놀랄때
        this.transform.rotation= Quaternion.AngleAxis(-2, this.transform.up);

        dist = Vector3.Distance(this.transform.position, mc.transform.position);
        if(dist<10)
        {
            brAnim.SetBool("PlayerInRange", true);
        }
    }
}
