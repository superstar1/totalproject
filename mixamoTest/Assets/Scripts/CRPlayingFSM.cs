﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//영상: crpoint에 있으면서 걷기 애니메이션만
// 일정 거리 이상 멀어지면 플레이어 바로 옆으로 순간이동
// 속도가 달라서 너무 멀어진다
// 계속 걸어. 플레이어가 일정 거리 안에 있을때만&& 플레이어가 멈추면(player 또는 그 자식의 rigidbody/charactercontroller에도 달려있음/으로 감지) 멈춰. 
public class CRPlayingFSM : MonoBehaviour
{

    enum crPState
    {
        CIdle, CWalk
    }

    crPState cr_PState;

    GameObject player, trash, CRpoint; //, crPlate;
    //Text crText;
    Animator crPAnim;

    float cTime, velocity, dist;
    public float crSpeed;
    Vector3 dir;
    Transform playerPos;
    public bool isPMoving = true;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");

        trash = GameObject.Find("Trash");

        CRpoint = GameObject.Find("CRPoint");

        //dist = Vector3.Distance(player.transform.position, this.transform.position);

        velocity = player.GetComponent<CharacterController>().velocity.magnitude;
        print(velocity);
        //crPlate = GameObject.Find("crPlate");
        //crText = GameObject.Find("crText").GetComponent<Text>();
        //crPlate.SetActive(false);

        crPAnim = GetComponent<Animator>();
        crPAnim.runtimeAnimatorController = Resources.Load("crPAnim") as RuntimeAnimatorController;

        dir = (trash.transform.position - this.transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up);
        transform.forward = dir;

        //crPAnim.SetBool("isCWalking", true);
        //cr_PState = crPState.CWalk;
        crPAnim.SetBool("isCIdle", true);
        cr_PState = crPState.CIdle;
    }

    // Update is called once per frame
    void Update()
    {
        // 계속 걸어.플레이어가 일정 거리 안에 있을때만 && 플레이어가 멈추면(player 또는 그 자식의 rigidbody / charactercontroller에도 달려있음 / 으로 감지) 멈춰.



        // 컨트롤러를 밀면 플레이어가 앞으로 이동한다.
        // 이동하는 상태인지 판단한다.
        Vector2 pInput = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        Vector2 sInput = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);

        float v = Input.GetAxis("Vertical");

        dist = Vector3.Distance(player.transform.position, this.transform.position);
        // if (pInput.y != 0 || sInput.y != 0 || v!=0)
        if (velocity == 0 && dist < 4)
        {
            isPMoving = false;
        }
        else
        {
            isPMoving = true;
        }


        switch (cr_PState)
        {
            case crPState.CIdle:
                CIdle();
                break;

            case crPState.CWalk:
                CWalk();
                break;

        }
    }

    private void CIdle()
    {
        crPAnim.SetBool("isCIdle", true);

        //print("State: " + cr_PState);

        if (isPMoving == true)
        {
            cr_PState = crPState.CWalk;
            crPAnim.SetBool("isCIdle", false);
        }
        else
        { }
    }

    private void CWalk()
    {
        // 플레이어가 걷고 있을 때 옆에서 같이 걸음

        if (isPMoving == true)
        {
            crPAnim.SetBool("isCWalking", true);

            //print("State: " + cr_PState);
            //this.transform.forward = player.transform.forward;

            Vector3 dir2 = (CRpoint.transform.position - this.transform.position).normalized;
            dir2 = Vector3.ProjectOnPlane(dir2, Vector3.up);
            //this.transform.position += this.transform.forward * crSpeed * Time.deltaTime;
            this.transform.position += dir2 * crSpeed * Time.deltaTime;
        }
        else if (isPMoving == false)
        {
            crPAnim.SetTrigger("CStops");
            cr_PState = crPState.CIdle;
            crPAnim.SetBool("isCWalking", false);
        }
        //else
        //{ }
    }
}
