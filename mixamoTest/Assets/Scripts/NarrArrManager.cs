﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NarrArrManager : MonoBehaviour
{
    GameObject arrow;
    Transform player, point;
    float cTime, dist;
    bool saidYes;
    GameObject choosecanvas = null;
    startUI startUI = null;
    closeUI closeUI = null;
    // Start is called before the first frame update
    void Start()
    {
        arrow = GameObject.Find("Arrow"); //이렇게 찾아놓고 셋액티브펄스 하면 나중에 찾을 수 있음
        arrow.gameObject.SetActive(false);
        player = GameObject.Find("Player").transform;
        point = GameObject.Find("WayPoint").transform;
        choosecanvas = GameObject.Find("ChooseCanvas");

        //choosecanvas.SetActive(false);
        startUI = choosecanvas.GetComponent<startUI>();

        //Vector3 dir = (point.position - arrow.transform.position).normalized;
        //dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
        //오브젝트의 xyz축이 내가 원하는 방향과 다를 땐, 내 마음대로 설정할 수 있는 빈 오브젝트를 만들어서 자식으로 넣어준다. 
        //arrow.transform.right = - dir;
        //arrow.transform.forward = dir;

        //살리기 arrow.SetActive(false);
        //arrow.GetComponent<MeshRenderer>().enabled = false; 메쉬렌더러가 부담 적은 건 사실, 하지만 스크립트 등이 붙어서 돌아간다면 다 꺼진다는 위험부담이 있음
    }

    // Update is called once per frame
    void Update()
    {
        cTime += Time.deltaTime;
        //나레이션 삽입 
        //yes ui 창 버튼 만들기 onclick


        //choosecanvas.SetActive(true);
        //startUI.starting();



        //choosecanvas.SetActive(false);
        if (!startUI.instance.isMax) // start버튼을 눌렀을 때 
        {
            Vector3 dir = (point.position - player.transform.position).normalized;
            dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
            arrow.transform.forward = dir;


            arrow.transform.position = player.position + -player.right * 0.4f + -player.up * 0f + player.forward * 7.0f; //월드좌표계를 쓰는 게 아니라 플레이어 기준으로 x y z 얼만큼 떨어져있는지 적어야 함
            arrow.gameObject.SetActive(true);
        }



        //dist = Vector3.Distance(arrow.transform.position, player.position);
        ////print("dist=" + dist);
        //if (dist < 5f)
        //{
        //    arrow.SetActive(false);
        //}
    }
}