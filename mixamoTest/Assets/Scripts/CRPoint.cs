﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEditor;
using UnityEngine;

public class CRPoint : MonoBehaviour
{
    public float speed = 0.2f;
    GameObject player;

    // 플레이어 주변을 천천히 돌아서 CR이 자연스럽게 플레이어를 따라오도록 한다.
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(player.transform.position, Vector3.down, speed * Time.deltaTime);
    }
}
