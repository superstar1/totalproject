﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueRobotClosing : MonoBehaviour
{
    // 주변을 둘러보며 감탄한다. "정말 아름답다"
    // 플레이어에게 고맙다고 인사한다. "고마워! 네 덕분에 ~"
    // 춤추는 친구 지켜봄
    // 친구가 말할 때 플레이어 봄
    // Start is called before the first frame update

    float currentTime;
    bool LegDelay2;
    GameObject br;
    Animator brCAnim;
    

    void Start()
    {
        br = GameObject.Find("BR");
        //br = GameObject.Find("AlphaforBR");
        brCAnim = br.GetComponent<Animator>();
        brCAnim.runtimeAnimatorController = Resources.Load("brCloAnim") as RuntimeAnimatorController;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        brCAnim.SetFloat("Time", currentTime);
        if(currentTime>3.9f && currentTime< 15)
        {
            brCAnim.SetBool("LegDelay2", true);
        }
        else
        {
            brCAnim.SetBool("LegDelay2", false);
        }
    }
}
