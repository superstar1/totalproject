﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HighlightingSystem;

public class BROpeningFSM : MonoBehaviour
{
    GameObject player,can, brPlate;
    Transform canPos, canFloatPos; //emptyHold
    //Text brText;
    Animator brAnim;
    float currentTime, dist;
    Vector3 dir;

    TypeWriterEffect typeWriterEffect = null;

    // 상태에 따라 다른 행동을 하도록 만든다
    // 각 상태를 선언하고 상태의 내용을 구성한다.

    enum brState
    {
        Idle, Walk, LiftandWonder, SurprisedandTalk
    }
    brState br_State;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        //emptyHold = GameObject.Find("EmptyforHold").transform;
        canPos = GameObject.Find("CanPos").transform;
        canFloatPos = GameObject.Find("CanFloatPos").transform;
        can = GameObject.Find("Can");
        can.SetActive(false);
        //can.GetComponent<MeshRenderer>().enabled= false;

        brAnim = GetComponent<Animator>();
        brAnim.runtimeAnimatorController = Resources.Load("brAnim") as RuntimeAnimatorController;

        brPlate = GameObject.Find("brPlate"); 
        typeWriterEffect = GameObject.Find("brText").GetComponent<TypeWriterEffect>();

        //brText = GameObject.Find("brText").GetComponent<Text>();

        // brText = GameObject.Find("brText").GetComponent<TextMeshProUGUI>();
        brPlate.SetActive(false);

        br_State = brState.Walk; 
    }
    
    private void Idle()
    {

    }
    private void Walk()
    {
        print("상태: Walk");

        if (currentTime < 5)
        {
            //dir = (can.transform.position - this.transform.position).normalized;
            dir = (canPos.transform.position - this.transform.position).normalized;
            dir = Vector3.ProjectOnPlane(dir, Vector3.up);
            transform.forward = dir;

            //if(dir.magnitude > 0.5f)
                //this.transform.position += dir * Time.deltaTime;
        }
        else if(5f < currentTime && currentTime < 6f)
        {
            brPlate.SetActive(true);

            typeWriterEffect.Get_Typing("어?  이게 뭐지?");
            //brText.text = "어?  이게 뭐지?";
        }
        else if (currentTime>6 && currentTime<11) //10
        {
            brPlate.SetActive(false);
            //if (currentTime > 8.5f)
            //{
            //    can.SetActive(true);
            //}
            br_State = brState.LiftandWonder;
        }
       
    }
    private void LiftandWonder()
    {
        print("상태: Walk-> LiftandWonder");

        if (12 < currentTime && currentTime < 15) //13 15
        {
            brAnim.SetBool("LegDelay", true);
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("우리 같은 로봇의 부품일까?"); // 너랑 모양이 비슷한 로봇의 부품일까?");
            //brText.text = "너랑 모양이 비슷한 로봇의 부품일까?";
        }
        //else
        //{
        //    brAnim.SetBool("LegDelay", false);
        //}
        else if(16<currentTime && currentTime<24) 
        {
            brPlate.SetActive(false);
            br_State = brState.SurprisedandTalk;
        }
    }
    private void SurprisedandTalk()
    {
        
        if(17.5f<currentTime)
        {
            can.transform.parent = null;
            // 하이라이트 활성화
            can.GetComponent<Highlighter>().enabled = true;
            // 로컬 x,y축 쪽으로 진동하면서 canFloatPos로 이동
            Vector3 temp = can.transform.forward * Mathf.Sin(Time.time * 5);
            can.transform.position = Vector3.Lerp(can.transform.position, canFloatPos.position, 1f * Time.deltaTime) + temp*0.001f;
        }
        //print("상태: LiftandWonder-> SurprisedandTalk");
        //this.transform.rotation = Quaternion.AngleAxis(-2, this.transform.up); //놀랄때
        
        if (18.5f < currentTime && currentTime < 19.5f) //17.5 19.5
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("깜짝이야!");
            //brText.text = "깜짝이야!"; 
        }
        else if(19.5f< currentTime && currentTime<21f) //19<
        {
            brPlate.SetActive(false);
        }
        else if(21f < currentTime && currentTime < 23f)
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("이게 도대체 뭘까?");
            //brText.text = "이게 도대체 뭘까?";
        }
        else if(23f < currentTime && currentTime< 26)
        {
            brPlate.SetActive(false);
        }
        else if (28f < currentTime && currentTime < 31f)
        {
            brAnim.SetBool("PlayerInRange", true);
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("안녕, 너 혹시 이게 뭔 줄 아니?");
            //brText.text = "안녕, 너 혹시 이게 뭔 줄 아니?";
        }
        //else if (dist < 2) //4

        // 컨트롤러를 잠그고, 플레이어 앞에 콜라이더를 나둬서 부딪히면 
        //dist = Vector3.Distance(this.transform.position, player.transform.position);
    }


    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        print("시간: " + currentTime);
        //can.transform.position = emptyHold.position;
        brAnim.SetFloat("Time", currentTime);

        if (currentTime > 8.4f)
        {
            can.SetActive(true);
        }

        switch (br_State)
        {
            case brState.Idle:
                Idle();
                break;

            case brState.Walk:
                Walk();
                break;

            case brState.LiftandWonder:
                LiftandWonder();
                break;

            case brState.SurprisedandTalk:
                SurprisedandTalk();
                break;
        }
    }
}
