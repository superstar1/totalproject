﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWhilePlaying : MonoBehaviour
{
    public float pWalkSpeed = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float v = Input.GetAxis("Vertical");
        this.transform.position += this.transform.forward * v * Time.deltaTime;
    }
}
