﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

//스테이트별 cTime 초기화했음
public class BRPlayingFSM : MonoBehaviour
{

    // 플레이어가 곁에 있을 때 걸어감
    // 플레이어가 일정 거리 이상 떨어지면 플레이어를 바라보며 =Idle
    // 일정 거리 안에 쓰레기 오브젝트가 있으면 멈춰섬+ 오브젝트를 가리킴+ 플레이어를 봄+ "저기 (오브젝트)가 있어" 

    enum brPState
    {
        FirstIdle, Idle, Walk, Point, Stop, Talk
    }
   
    brPState br_PState;

    GameObject player, trash, brPlate;
    Text brText;
    Animator brPAnim;

    TypeWriterEffect typeWriterEffect = null; 

    float cTime;
    Vector3 dir;
    bool waittoTalk;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");

        //trash = GameObject.Find("Trash");
        trash = GameObject.Find("can 2");

        //brText = GameObject.Find("brText").GetComponent<Text>();
        brPlate = GameObject.Find("brPlate");
        typeWriterEffect = GameObject.Find("brText").GetComponent<TypeWriterEffect>();

        brPAnim = GetComponent<Animator>();
        brPAnim.runtimeAnimatorController = Resources.Load("brPAnim") as RuntimeAnimatorController;
        brPlate.SetActive(false);

        //dir = (trash.transform.position - this.transform.position).normalized;
        //transform.forward = dir;
        //brPAnim.SetBool("isWalking", true);
        //brPAnim.SetBool("isIdle", true);
        //br_PState = brPState.Walk;
        br_PState = brPState.FirstIdle;
        cTime = 0; //여기부터
    }

    // Update is called once per frame
    void Update()
    {
        print(br_PState);

        if (startUI.instance.clickedYes)
        {
            switch (br_PState)
            {
                case brPState.FirstIdle:
                    FirstIdle();
                    break;
                case brPState.Idle:
                    Idle();
                    break;
                case brPState.Walk:
                    Walk();
                    break;
                case brPState.Point:
                    Point();
                    break;
                //case brPState.Stop:
                //    Stop();
                //    break;
                case brPState.Talk:
                    Talk();
                    break;
            }
        }
        else
        {
            Idle();
        }

    }
    
    private void FirstIdle()
    {

        cTime += Time.deltaTime;
        //print("FirstIdle경과시간: " + cTime);
        if (cTime < 1f) //애니메이션 프레임에서 시간을 찾아와서 제어해야 함
        {
            Vector3 dir2 = player.transform.position - this.transform.position;
            dir2 = Vector3.ProjectOnPlane(dir2.normalized, Vector3.up);
            this.transform.forward = dir2;
            //brPAnim.SetBool("isTurning", true);
        }
        else if (cTime>1f && cTime<4f)
        {
            //brPAnim.SetBool("isTurning", false);
            //brPAnim.SetBool("isTurning", true);
            dir = trash.transform.position - this.transform.position;
            dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir.normalized), 1.4f * Time.deltaTime);
        }
        else if (cTime > 4f)
        {
            //brPAnim.SetBool("isTurning", false);
            print(brPAnim);
            brPAnim.SetBool("isPlayStart", true);
            br_PState = brPState.Walk;
            cTime = 0;
        }
    }

    private void Idle()
    {
        print("상태= Idle");
        brPAnim.SetBool("isIdle", true);
        //print("Idle 진입");
        // 위치 제자리+ 플레이어를 바라봄
        cTime+= Time.deltaTime;
        if (cTime> 1.0f && cTime< 3.0f) //3 5
        {
            brPAnim.SetBool("isTurning", true);
        }
        if(cTime<=1.0f || cTime>=3.0f) //3 5 
        {
            brPAnim.SetBool("isTurning", false);
        }

       if (cTime > 3.0f) //5
       {
            //brPAnim.SetBool("isTurning", true);

            Vector3 dir2 = player.transform.position - this.transform.position;
            dir2 = Vector3.ProjectOnPlane(dir2.normalized, Vector3.up);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir2), 1.3f * Time.deltaTime);
            //cTime = 10;
            //cTime = 0;
       }

        float plDist = Vector3.Distance(player.transform.position, this.transform.position);
        float pltrDist = Vector3.Distance(trash.transform.position, player.transform.position);

        if (plDist < 4.0f && waittoTalk== false)
        {
            dir = (trash.transform.position - this.transform.position).normalized;
            //transform.forward = dir;
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir), 1.4f * Time.deltaTime);
            br_PState = brPState.Walk;
            cTime = 0;
        }
        if (pltrDist<6 && waittoTalk == true)  //4
        {
            dir = (trash.transform.position - this.transform.position).normalized;
            transform.forward = dir;
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir), 5f * Time.deltaTime);
            br_PState = brPState.Point;
            cTime = 0; 
            brPAnim.SetBool("isIdle", false);
        }


    }
    private void Walk()
    {
        print("상태= Walking");
        dir = (trash.transform.position - this.transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up);
        transform.forward = dir;
        brPAnim.SetBool("isWalking", true);
        // 플레이어가 옆에 있을 때 앞으로 걸어감
        float plDist = Vector3.Distance(player.transform.position, this.transform.position);
        //print("plDist=" + plDist);
        if (plDist > 10.0f)
        {
            brPAnim.SetTrigger("Stops");
            br_PState = brPState.Idle;
            cTime = 0;
            brPAnim.SetBool("isWalking", false);
        }

        float trDist = Vector3.Distance(trash.transform.position, this.transform.position);
        float pltrDist= Vector3.Distance(trash.transform.position, player.transform.position);

        //print("로봇거리=" + trDist);
        //print("플레이어거리=" + pltrDist);

        if(trDist < 6.0f)
        {
            if(pltrDist < 9.0f) //6
            {
                brPAnim.SetTrigger("Stops");
                br_PState = brPState.Point;
                cTime = 0;
                brPAnim.SetBool("isWalking", false);
            }
            else if(pltrDist > 9.0f) //6
            {
                brPAnim.SetTrigger("Stops");
                br_PState = brPState.Idle;
                cTime = 0;
                waittoTalk = true;
                brPAnim.SetBool("isWalking", false);
            }
        }

    }
    private void Point()
    {
        // 오브젝트의 트랜스폼을 가리킴
        cTime += Time.deltaTime;
        print("State: Point");
        //brPAnim.SetTrigger("Points");
        dir = (trash.transform.position - this.transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up);
        transform.forward = dir;
        brPAnim.SetTrigger("PointsinIdle");
        // UI 넣고 여기서 오브젝트 이름 바꾸기
        if (3.5f < cTime && cTime < 6) 
        {
        brPlate.SetActive(true);
        typeWriterEffect.Get_Typing("앗! 저기에 다른 쓰레기가 있어.");
       
        }
        else if (6 < cTime)
       {
        brPlate.SetActive(false);
        br_PState = brPState.Talk;
           cTime = 0;
        }

    }
    private void Talk()
    {
        cTime += Time.deltaTime;
        print("State:Talk");
        // 플레이어를 바라봄+ 말함
        brPAnim.SetBool("isTalking", true);
        if (0f < cTime && cTime< 2f)
        {
            brPAnim.SetBool("isTurning", true);
        }
        if (cTime >2f)
        {
            brPAnim.SetBool("isTurning", false);
        }

        Vector3 dir = player.transform.position - this.transform.position;
        dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir.normalized), 1.0f * Time.deltaTime);
        if (2< cTime && cTime < 5)
        {
            brPlate.SetActive(true);
            //brText.text = "저건 어떻게 분리수거해야 해?"; 
            typeWriterEffect.Get_Typing("저건 어떻게 분리수거해야 해?");
        }
        if (5 < cTime)
        {
            brPlate.SetActive(false);
        }
    }

}
