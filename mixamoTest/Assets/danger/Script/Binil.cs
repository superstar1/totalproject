﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Binil : MonoBehaviour
{
    Vector3 xz_temp;
    Vector3 temp;
    GameObject binil;
    void Start()
    {
        binil = GameObject.Find("Brand");
    }
    // Update is called once per frame
    void Update()
    {


        xz_temp = transform.localPosition;
        xz_temp.z = -4.68f;
        xz_temp.x = 0;
        transform.localPosition = xz_temp;
        if (transform.localPosition.y < 0)
        {
            Vector3 temp = transform.localPosition;
            temp.y = 0;
            transform.localPosition = temp;
        }
        else
        {
            if (binil.GetComponent<CurveShapeDeformer1>().Multiplier > 8)
            {
                transform.parent = null;
                binil.GetComponent<CurveShapeDeformer1>().Multiplier = 0;
                binil.transform.parent = null;
                binil.AddComponent<Rigidbody>().useGravity = false;
                binil.GetComponent<OVRGrabbable>().enabled = true;
                binil.transform.position = transform.position;
                Destroy(gameObject);
                return;
            }
            else
            {
                this.gameObject.transform.parent.GetComponent<CurveShapeDeformer1>().Multiplier = this.transform.localPosition.y;
            }
        }
        if (OVRInput.Get(OVRInput.Button.One))
        {
            temp = transform.position;
            temp.y = GameObject.Find("RightHandAnchor").transform.position.y;
            transform.position = temp;
        }
    }
}
