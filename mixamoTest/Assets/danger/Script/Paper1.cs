﻿using Epitybe.VRInventory;
using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paper1 : MonoBehaviour
{
    Vector3 newPos;
    int count = 1;
    int child;

    private void Update()
    {
        if (count == 3)
        {
            transform.parent.GetComponent<Highlighter>().tween = false;
            transform.parent.GetComponent<Highlighter>().constant = true;
            transform.parent.GetComponent<Highlighter>().filterMode = RendererFilterMode.Include;
            transform.parent.GetComponent<Highlighter>().filterMode = RendererFilterMode.None;
            transform.parent.gameObject.AddComponent<Storable>();
            transform.parent.gameObject.AddComponent<MakeLeaf>();
            count++;
        }
        else if (transform.parent.GetComponent<Rigidbody>().isKinematic)
        {
            transform.parent.GetComponent<Highlighter>().tween = false;
        }
        else if (count > 3)
        {
            return;
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "Down")
        {
            Destroy(collision.gameObject.transform.parent.gameObject);

            gameObject.GetComponent<BoxCollider>().center += new Vector3(0, 1, 0);
            transform.parent.GetComponent<BoxCollider>().center += new Vector3(0, 0.5f, 0);
            transform.parent.GetComponent<BoxCollider>().size += new Vector3(0, 1, 0);


            GameObject paper = Instantiate(Resources.Load<GameObject>("Paper"));

            transform.parent.GetComponent<Highlighter>().filterList.Add(paper.transform);

            paper.transform.parent = transform.parent;
            paper.transform.rotation = Quaternion.identity;
            paper.transform.localPosition = new Vector3(0, count, 0);
            Quaternion q = Quaternion.AngleAxis(Random.Range(5f, 10f), Vector3.up);
            paper.transform.rotation = q * paper.transform.rotation;
            count++;
        }

    }
}
