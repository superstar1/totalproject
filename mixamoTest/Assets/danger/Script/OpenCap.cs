﻿using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCap : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public bool isRotate;
    Quaternion preRotate;
    bool divide = true;
    Vector3 temp;
    Vector3 temp1;
    void Update()
    {
        if (transform.parent == null)
        {
            print("1");
            GetComponent<Highlighter>().tween = false;
            GetComponent<Highlighter>().constant = true;
        }
        else if (transform.parent.GetComponent<Rigidbody>().isKinematic)
        {
            GetComponent<Highlighter>().tween = true;
        }
        else
            GetComponent<Highlighter>().tween = false;



        if (Quaternion.Angle(preRotate, transform.localRotation) > 0)
        {
            isRotate = true;
        }
        else
            isRotate = false;

        preRotate = transform.localRotation;
        if (divide)
        {
            if (transform.localPosition.y >= -0.16f)
            {
                transform.parent = null;
                isRotate = false;
                //GameObject.Find("RightHandAnchor").GetComponent<OpenCaper>().enabled = false;
                transform.position = temp1;
                gameObject.AddComponent<Rigidbody>().useGravity = false;
                gameObject.GetComponent<OVRGrabbable>().enabled = true;
                OpenCaper.opened = true;
                divide = false;
            }
            else if (isRotate)
            {
                temp = transform.localPosition;

                temp.y += 0.01f;
                transform.localPosition = temp;
                temp1 = transform.position;
            }
        }
    }

}
