﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paper : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "Down")
        {
            collision.transform.parent.transform.position
                    = transform.parent.GetChild(0).transform.position;
            transform.parent.transform.parent = collision.transform.parent.transform;
            transform.parent.transform.rotation = Quaternion.Euler(0, 0, 0);
            Quaternion q = Quaternion.AngleAxis(Random.Range(5f, 10f), transform.up);
            collision.transform.parent.rotation = q * collision.transform.parent.rotation;
            for (int i = 0; i < 2; i++)
            {
                transform.parent.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

    }
}
