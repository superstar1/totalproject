﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserOnOff : MonoBehaviour
{
    public GameObject laser;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two) && laser.activeSelf)
        {
            laser.SetActive(false);
        }
        else if(OVRInput.GetDown(OVRInput.Button.Two) && laser.activeSelf == false)
        {
            laser.SetActive(true);
        }
    }
}
