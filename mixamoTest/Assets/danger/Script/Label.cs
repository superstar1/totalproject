﻿using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Label : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.parent == null)
        {
            GetComponent<Highlighter>().tween = false;
            GetComponent<Highlighter>().constant = true;
        }
        else if (transform.parent.GetComponent<Rigidbody>().isKinematic)
        {
            GetComponent<Highlighter>().tween = true;
        }
        else
            GetComponent<Highlighter>().tween = false;
    }
}
