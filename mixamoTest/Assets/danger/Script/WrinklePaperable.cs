﻿using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrinklePaperable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    void Update()
    {
        if (GetComponent<RippleDeformer>().PeakMultiplier <= 0.1f)
        {
            gameObject.GetComponent<Highlighter>().tween = false;
            gameObject.GetComponent<Highlighter>().constant = true;
            return;
        }
        
        if (gameObject.GetComponent<Rigidbody>().isKinematic)
        {
            gameObject.GetComponent<Highlighter>().tween = false;
        }
        else
            gameObject.GetComponent<Highlighter>().tween = true;
    }

}
