﻿using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Can : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        hand = GameObject.Find("RightHandAnchor");
    }
    GameObject hand;
    Vector3 temp;
    Vector3 buttonDownPos;
    Vector3 dir;
    // Update is called once per frame
    void Update()
    {
        if (transform.parent.gameObject.GetComponent<Rigidbody>().isKinematic)
        {
            transform.parent.GetComponent<Highlighter>().tween = false;
        }
        else
            transform.parent.GetComponent<Highlighter>().tween = true;

        if (compress)
        {
            if (OVRInput.GetDown(OVRInput.Button.One))
            {
                buttonDownPos = hand.transform.position;
            }

            if (OVRInput.Get(OVRInput.Button.One))
            {
                dir = hand.transform.position - buttonDownPos;
                if (dir.magnitude > 0)
                {
                    if (transform.parent.transform.localScale.y <=0.2f)
                    {
                        transform.parent.GetComponent<Highlighter>().tween = false;
                        transform.parent.GetComponent<Highlighter>().constant = true;
                        return;
                    }
                    temp = transform.parent.transform.localScale;
                    temp.y -= 0.005f;
                    transform.parent.transform.localScale = temp;
                }
            }
        }
    }
    bool compress;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "RightHandAnchor")
        {
            compress = true;
        }
    }
}
