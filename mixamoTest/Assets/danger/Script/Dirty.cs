﻿using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirty : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public bool isMoving;
    Vector3 prePos;
    public float test;
    void Update()
    {
        if (gameObject.GetComponent<Renderer>().material.color.a <= 0.05f)
        {
            GetComponent<Highlighter>().tween = false;
            GetComponent<Highlighter>().constant = true;
            StopAllCoroutines();
        }
        else if (GetComponent<Rigidbody>().isKinematic)
        {
            GetComponent<Highlighter>().tween = false;
        }
        else
            GetComponent<Highlighter>().tween = true;
        
        if (Vector3.Distance(prePos, this.transform.position) > test)
        {
            isMoving = true;
        }
        else
            isMoving = false;

        prePos = this.transform.position;
    }
}
