﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCaper : MonoBehaviour
{
    GameObject handangle;
    void Start()
    {
        handangle = GameObject.Find("RightHandAnchor");
    }

    Quaternion temp;
    Quaternion temp1;
    Quaternion temp2;
    float degree;
    void Update()
    {
        if (canOpen && !opened && dduggung.transform.parent !=null)
        {
            if (OVRInput.GetDown(OVRInput.Button.Four))
            {
                temp1 = handangle.transform.rotation;
            }

            if (OVRInput.Get(OVRInput.Button.Two) || OVRInput.Get(OVRInput.Button.Four))
            {
                temp2 = handangle.transform.rotation;
                degree = temp2.y - temp1.y;
                temp = dduggung.transform.localRotation;
                temp.y = -degree;
                dduggung.transform.localRotation *= temp ;
            }
        }
    }
    public bool canOpen;
    public static bool opened;
    GameObject dduggung;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "lid")
        {
            canOpen = true;
            dduggung = other.gameObject;
        }
    }
}
