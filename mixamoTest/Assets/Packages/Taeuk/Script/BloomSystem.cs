﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BloomSystem : MonoBehaviour
{
    public GameObject[] treeList;
    public int NumOfTrees = 20;
    public GameObject[] flowerList;
    public int NumOfFllowers = 200;
    public GameObject[] stoneList;
    public int NumOfStones = 200;
    public GameObject[] grassList;
    public int NumOfGrass = 200;
    public GameObject[] bushList;
    public int NumOfBush = 200;
    int index = 0;

    List<Vector3> list = new List<Vector3>();
    LineRenderer lr;
    public GameObject loadPlane;
    Transform bloomPos;
    Vector3 dir;
    public float radius = 20f;
    public static bool go;

    // Start is called before the first frame update
    void Start()
    {

        lr = this.GetComponent<LineRenderer>();

        bloomPos = GameObject.Find("SpawnPosition").transform;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            GameObject parentGOD = new GameObject("ParentList");

            BloomItems(parentGOD, "Tree", treeList, NumOfTrees);
            BloomItems(parentGOD, "Flower", flowerList, NumOfFllowers);
            BloomItems(parentGOD, "Stone", stoneList, NumOfStones);
            BloomItems(parentGOD, "Grass", grassList, NumOfGrass);
            BloomItems(parentGOD, "Bush", bushList, NumOfBush);


            CreathPath();
            go = false;
        }
    }

    void BloomItems(GameObject parentGOD, string changeName, GameObject[] list, int Number)
    {
        GameObject parent = new GameObject(changeName + "Parent");

        for (int i = 0; i < Number; i++)
        {
            index++;
            if (index == list.Length)
                index = 0;

            GameObject target = Instantiate(list[index]);

            dir = bloomPos.transform.position + new Vector3(Random.Range(-radius, radius), 0, Random.Range(-radius, 0));

            print(dir);

            target.transform.position = dir;
            target.AddComponent<Bloom>();
            target.gameObject.name = changeName;
            target.transform.parent = parent.transform;
        }
        index = 0;

        parent.transform.parent = parentGOD.transform;
    }


    void CreathPath()
    {
        if (Time.frameCount % 2 == 0 && Input.GetButtonDown("Fire1"))
        {
            list.Add(this.transform.position);

            if (loadPlane)
            {
                GameObject road = Instantiate(loadPlane);
                road.transform.position = this.transform.position + (-this.transform.forward) * 3f;
            }
        }

        if (lr)
        {
            lr.positionCount = list.Count;
            lr.SetPositions(list.ToArray());
        }


        //list.Clear();
    }
}