﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloom : MonoBehaviour
{
    Transform player;
    Vector3 dir;
    [Header("Tree")]
    const int MaximumTime = 4;
    float lastTime = 0;
    float currentTime = 0;
    float zoomTime = 1f;
    [Range(0, 15)]
    public int test = 0;
    [Header("Flowdsafsdf")]
    public int test2 = 0;
    float mag = 1f;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
        this.transform.localScale = (Vector3.one * 0.1f);

        if (this.gameObject.name == "Tree")
        {
            
            mag = 0.1f;
            zoomTime = 13f;
        }
        else if (this.gameObject.name == "Flower")
        {
            gameObject.transform.localScale += Vector3.one * 5f;
            gameObject.transform.position += Vector3.up * 0.5f;
            zoomTime = 13f;
        }
        else if (this.gameObject.name == "Stone")
            zoomTime = 8f;
        else if (this.gameObject.name == "Grass")
            zoomTime = 6f;
        else if (this.gameObject.name == "Bush")
            zoomTime = 6f;
    }
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime > 5f)
        {
            //this.transform.localScale = Vector3.Lerp(this.transform.localScale * 0.1f, this.transform.localScale * zoomRate, 0.1f);

            // 3배로 커지기 전까지 실행
            if (lastTime != currentTime)
            {
                lastTime = currentTime;
                this.transform.localScale += (Vector3.one * mag * Time.deltaTime); // 시간만큼 커진다
                
                // zoomtime이 되면 스크립트 해제
                if (lastTime > zoomTime)
                {
                    Destroy(this.GetComponent<Bloom>());
                }
            }
        }
    }
}
