﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 4가지 종류(플라스틱, 병, 캔, 종이)의 물체를 담을 수 있는 쓰레기통에 물체를 담고, 그 개수를 센다.
// 필요 속성 : 4가지 종류 물체와 각각의 개수를 저장할 수 있는 변수
public class RecyclingSystem : MonoBehaviour
{

    public string[] tagofBins = { "Plastic", "Bottle", "Can", "Paper" };
    public int[] NumofRecycled = new int[4];
    public List<GameObject> list_recycleBins;
    public static RecyclingSystem instance;

    // 재활용시스템의 자식인 4개의 쓰레기통을 리스트에 담는다.
    private void Awake()
    {
        instance = this;

        foreach (Transform child in transform)
        {
            list_recycleBins.Add(child.gameObject);
        }

        for (int i = 0; i < list_recycleBins.Count; i++)
        {
            list_recycleBins[i].tag = tagofBins[i];
            list_recycleBins[i].AddComponent<Bin>();
        }
    }
    private void Start()
    {

    }
}
