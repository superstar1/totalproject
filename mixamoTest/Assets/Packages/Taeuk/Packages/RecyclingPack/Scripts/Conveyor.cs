﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 물체가 닿으면 물체를 플레이어 방향(Back)으로 이동시킨다.
// 닿는 물체마다 이동스크립트를 넣어준다.
public class Conveyor : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {   
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag != "Player")
            other.gameObject.transform.position += (-transform.forward) * 5f * Time.deltaTime;
    }
}
