﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

// 스페이스 버튼을 누르면 무기를 90도 회전한 후 일정 시간 후 기존 각도로 되돌아온다.
// 필요 속성 : 회전각도, 기존 각도
public class BeatRecycle : MonoBehaviour
{
    GameObject weapon;
    Transform originPos;
    Quaternion state2;
    int isAttacking;
    float currentTime;
    float attackDoneTime = 0.5f;
    public float speed = 5f;
    public float camDistance = 3f;
    public float camHeight = 3f;
    public float camAngle = 30f;
    public LineRenderer lineRenderer;
    Vector3 mouseOffset;
    float zCoord;
    RaycastHit hitGO;
    public float rotSpeed = 5f;
    public float lerpRatioPerFrame = 0.1f;
    GameObject cam;
    

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("Main Camera");

        if (GameObject.Find("WeaponPlace")) {
            weapon = GameObject.Find("WeaponPlace").gameObject;
            originPos = weapon.transform;
        }
        
        if (GetComponent<LineRenderer>())
        {
            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.startWidth = .05f;
            lineRenderer.endWidth = .05f;
        }

    }

    // Update is called once per frame
    void Update()
    {
        //Attack();
        //Ray();
    }

    private void FixedUpdate()
    {
        Move();
        FollowCamera();
    }


    void Ray()
    {
        // 마우스 커서 입력을 받아 스크린에 레이를 만든다.
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
        RaycastHit hit;

        // 만약 최대 100의 길이의 레이가 물체에 맞으면 hitInfo를 저장, 라인을 Scene View에 그린다.
        if (Physics.Raycast(ray, out hit, 2))
            Debug.DrawLine(ray.origin, hit.point);

        // 라인렌더러를 이용하여 Game View에 플레이어와 마우스가 클릭하는 곳 까지 선을 그린다.
        lineRenderer.SetPosition(0, this.transform.position);
        lineRenderer.SetPosition(1, hit.point);
    }


    private void Move()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 dir =/* h * transform.right + */v * transform.forward;

        transform.position += dir * speed * Time.deltaTime;
        this.transform.rotation = Quaternion.AngleAxis(h * rotSpeed, this.transform.up) * transform.rotation;
        cam.transform.rotation = this.transform.rotation/* * Quaternion.AngleAxis(camAngle, Camera.main.transform.right)*/;

    }



    void FollowCamera()
    {
        Vector3 locatePos = this.transform.up * camHeight + (-this.transform.forward * camDistance);
        cam.transform.position = Vector3.Lerp(cam.transform.position + locatePos, this.transform.position, lerpRatioPerFrame);
        //Camera.main.transform.rotation = Quaternion.AngleAxis(camAngle, Camera.main.transform.right);
    }

    void Attack()
    {
        // 스페이스 버튼을 누르면 무기를 90도 회전한 후 일정 시간 후 기존 각도로 되돌아온다.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isAttacking = 1;
        }

        if (isAttacking == 1)
        {
            weapon.transform.rotation = Quaternion.Lerp(originPos.rotation, Quaternion.Euler(90f, 0.0f, 0f) * this.gameObject.transform.rotation, 0.2f);

            currentTime += Time.deltaTime;
            if (currentTime > attackDoneTime)
            {
                currentTime = 0;
                isAttacking = 2;
            }
        }

        if (isAttacking == 2)
        {
            weapon.transform.rotation = Quaternion.Lerp(originPos.rotation, Quaternion.Euler(-90f, 0.0f, 0f) * this.gameObject.transform.rotation, 0.2f);

            currentTime += Time.deltaTime;
            if (currentTime > attackDoneTime)
            {
                currentTime = 0;
                isAttacking = 0;
            }
        }
    }
}
