﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    Vector3 mouseOffset;
    float zCoord;
    float distance;

    private void OnMouseDown()
    {

        // Store offset = gameobject world pos - mouse world pos
        
        Vector3 dir = GameObject.Find("Player").transform.position - this.transform.position;
        distance = dir.magnitude;

        // 3이내에 있으면 클릭가능
        if (distance < 10f)
        {
            zCoord = Camera.main.WorldToScreenPoint(this.transform.position).z;

            
            mouseOffset = this.transform.position - GetMouseWorldPos();

        }


    }

    private Vector3 GetMouseWorldPos()
    {
        // pixel coordinate(x, y)
        Vector3 mousePos = Input.mousePosition;

        // z coordinate of gameobject on screen
        mousePos.z = zCoord;

        return Camera.main.ScreenToWorldPoint(mousePos);
    }

    private void OnMouseDrag()
    {
        // 3이내에 있으면 드래그 가능
        if (distance < 2f)
        {
            this.transform.position = GetMouseWorldPos() + mouseOffset;
        }
    }
}
