﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bin : MonoBehaviour
{
    public bool isOpening = false;
    Transform childLid;
    GameObject explosionParticle;
    
    private void Start()
    {
        childLid = this.transform.GetChild(0);

    }

    private void Update()
    {
        if (isOpening)
        {
            // 로컬 회전양을 X축 기준으로 -90만큼 회전
            Quaternion openAngle =  Quaternion.AngleAxis(-80f, Vector3.right);
            childLid.localRotation = Quaternion.Lerp(childLid.localRotation, openAngle, 0.5f);

            float angle = Quaternion.Angle(childLid.localRotation, openAngle); // childLid.localRotation에서 newQ 사이의 각도 확인

            if (angle == 0)
            {
                isOpening = false;
            }
        }
        else
        {
            Quaternion closeAngle = Quaternion.AngleAxis(0, Vector3.right);
            childLid.localRotation = Quaternion.Lerp(childLid.localRotation, closeAngle, 0.3f);
        }
    }

    // 쓰레기의 이름과 쓰레기통의 이름이 매치되면 아이템이 파괴되고, NumofRecycled의 변수 개수가 올라간다.
    private void OnTriggerEnter(Collider other)
    {
        // 1. 충돌체의 이름(Can)과 쓰레기통의 태그(Can)가 일치하면,
        if (other.gameObject.name == this.gameObject.tag)
        {
            // 먼지효과 내기
            explosionParticle = Instantiate(Resources.Load<GameObject>("CFX_MagicPoof"));
            explosionParticle.transform.position = childLid.position;
            explosionParticle.GetComponent<ParticleSystem>().Play();

            isOpening = true;
            // 2. 아이템이 0.1크기만큼 작아지며 2초 후 파괴
            other.gameObject.transform.localScale = other.gameObject.transform.localScale * 0.1f;
            other.gameObject.transform.position = childLid.position;
            Destroy(other.gameObject, 2f);

            // 3. 개수가 올라간다.
            if(RecyclingSystem.instance.tagofBins[0] == this.gameObject.tag)
                RecyclingSystem.instance.NumofRecycled[0]++;

            if (RecyclingSystem.instance.tagofBins[1] == this.gameObject.tag)
                RecyclingSystem.instance.NumofRecycled[1]++;

            if (RecyclingSystem.instance.tagofBins[2] == this.gameObject.tag)
                RecyclingSystem.instance.NumofRecycled[2]++;

            if (RecyclingSystem.instance.tagofBins[3] == this.gameObject.tag)
                RecyclingSystem.instance.NumofRecycled[3]++;
        }
    }
}
