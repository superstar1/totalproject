﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

// 쓰레기 공장에서 쓰레기 더미를 만든다.
// 쓰레기 개수
public class GarbageFactory : MonoBehaviour
{
    string[] nameOfMaterial = { "Plastic", "Bottle", "Can", "Paper" };
    public GameObject garbage;
    public int NumofGarbages = 10;
    public int randomRadius = 15;
    public int randomHeight = 100;
    private float currentTime;
    public float createTime = 3f;

    // Start is called before the first frame update
    void Start()
    {
        // NumofGarbages 만큼의 쓰레기 더미를 만든다.
        //for (int i = 0; i < NumofGarbages; i++)
        //{
        //    var garbageClone = Instantiate(garbage, this.transform.position
        //        + new Vector3(Random.Range(-randomRadius, randomRadius), Random.Range(0, 100), Random.Range(-randomRadius, randomRadius))
        //        , Quaternion.identity);
        //    garbageClone.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 0f, 1f, 0.5f, 1f); // 쓰레기를 랜덤한 HSV값으로 설정해준다.
        //    garbageClone.AddComponent<Rigidbody>();
        //    garbageClone.layer = 8; // Garbage로 레이어 설정
        //    garbageClone.name = nameOfMaterial[Random.Range(0, 4)];
        //    garbageClone.transform.SetParent(this.transform);
        //}
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime > createTime)
        {
            var garbageClone = Instantiate(garbage, this.transform.position + new Vector3(Random.Range(-2, 2), 0, 0), Quaternion.identity);
            garbageClone.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 0f, 1f, 0.5f, 1f); // 쓰레기를 랜덤한 HSV값으로 설정해준다.
            garbageClone.AddComponent<Rigidbody>();
            garbageClone.layer = 8; // Garbage로 레이어 설정
            garbageClone.name = nameOfMaterial[Random.Range(0, 4)];
            garbageClone.transform.SetParent(this.transform);

            currentTime = 0;
        }
    }
}
