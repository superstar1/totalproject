﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8 && !other.gameObject.GetComponent<DragObject>())
            other.gameObject.AddComponent<DragObject>();
    }

}
