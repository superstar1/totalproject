﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 15f;
    public float height = 3f;
    public float distance = 3f;
    GameObject cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {

        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        Vector3 dir = (v) * this.transform.right + h * this.transform.forward;

        this.transform.position += dir * speed * Time.deltaTime;
        //this.transform.rotation = Quaternion.LookRotation(cam.transform.forward);
        cam.transform.position = this.transform.position/* + this.transform.up * height + (-this.transform.forward * distance)*/;
        

    }


}
