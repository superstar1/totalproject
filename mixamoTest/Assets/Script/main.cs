﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class main : MonoBehaviour
{
    public static GameObject preshell, prefExplosion,prefETank;



    void Start()
    {
        preshell = Resources.Load<GameObject>("Shell 1");
        // 로딩 시간을 줄이기 위해 먼저 받아와서 생성한다?
        prefExplosion = Resources.Load<GameObject>("fx_fumefx_boom_01");
        prefETank = Resources.Load<GameObject>("TankE");
        //GameObject.Find("Plane").GetComponent<Renderer>().material.color = Color.red;
        //GameObject.Find("Plane").transform.localScale = new Vector3(1, 1, 1) * 5f;
        BoxCollider collider = GameObject.Find("Tank").AddComponent<BoxCollider>();
        collider.size = new Vector3(2, 2, 2);
        collider.center = new Vector3(0, 1, 0);

        StartCoroutine("mkEnemy");
    }

    IEnumerator mkEnemy()
    {
        while (true)
        {
            GameObject g = Instantiate(main.prefETank);

            g.transform.position = new Vector3(Random.Range(-10f, 10f), 1, Random.Range(-10f, 10f));
            g.transform.rotation = Quaternion.identity;

            Renderer[] rend = g.transform.GetChild(0).GetComponentsInChildren<Renderer>();
            foreach (Renderer r in rend)
            {
                r.material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            }

            //g.AddComponent<Rigidbody>();
            BoxCollider collider = g.AddComponent<BoxCollider>();
            collider.size = new Vector3(2, 2, 2);
            collider.center = new Vector3(0, 1, 0);


            //yield return new WaitForSeconds(Random.Range(3f, 10f));
            yield return new WaitForSeconds(100f);
        }
    }


    // Update is called once per frame
    void Update()
    {

    }
}
