﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour
{
    public static player _this;
    public Image shotAim;
    Rigidbody rb;
    public Transform firePos;
    public GameObject Turret;
    GameObject AA;
    public Image imgHp;
    // Start is called before the first frame update
    void Start()
    {
        _this = this;
        hp = max_hp;
        BoxCollider collider = this.gameObject.AddComponent<BoxCollider>();
        collider.size = new Vector3(2, 2, 2);
        collider.center = new Vector3(0, 1, 0);
        rb = this.gameObject.AddComponent<Rigidbody>();
        AA = this.transform.GetChild(0).gameObject;
        AA.SetActive(false);
        //GameObject.Find("Aim").GetComponent<Image>().sprite = Resources.Load<Sprite>("KakaoTalk_20200916_162609694");
        //imgHp = GameObject.Find("Hp").GetComponent<Image>();
    }
    Vector3 worldPos;
    float hp;
    float max_hp = 100f;
    // Update is called once per frame
    void Update()
    {
        //firePos.position = Turret.transform.forward + new Vector3(0, 1f, 1f);
        //firePos.rotation = Turret.transform.rotation;
        //firePos.rotation = Turret.transform.rotation;
        if (Input.GetKeyDown(KeyCode.X))
        {
            this.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * 30f, ForceMode.VelocityChange);
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            turretRotate();
        }
        else if (!Input.GetKeyDown(KeyCode.LeftControl))
        {
            move();
        }
        //fire();
        gimoa();
        //camAction();
        //hp -= Time.deltaTime;
        imgHp.fillAmount = hp / max_hp;
        DamageRed();
        //worldPos = Camera.main.WorldToScreenPoint(firePos.transform.position);
        //shotAim.transform.position = worldPos;
        if (hp < 0)
        {
            Destroy(gameObject);
            GameLabel = GameObject.Find("GameLabel").GetComponent<Text>();
            GameLabel.text = "GameOver";
        }

    }
    float damageRedValue;
    Text GameLabel;
    void DamageRed()
    {
        if (damageRedValue < 0) damageRedValue = 0;

        GameObject.Find("DamageRed").GetComponent<Image>().color = new Color(1, 0, 0, damageRedValue);
        damageRedValue -= Time.deltaTime;
    }
    IEnumerator DamageRotate()
    {
        Quaternion q = cam.rotation;

        for (int i = 0; i < 100; i++)
        {
            Vector3 rand = new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f)
                , Random.Range(-2f, 2f));
            cam.rotation = Quaternion.AngleAxis(Random.Range(-2f, 2f), rand.normalized) * cam.rotation;
            yield return new WaitForSeconds(0.001f);
            
        }

        cam.rotation = q;
    }
    public void Damage(float v)
    {
        damageRedValue = 0.5f;
        this.hp -= v;
        StartCoroutine(DamageRotate());
    }
    void turretRotate()
    {
        float h = Input.GetAxis("Horizontal");
        //GameObject Turret = transform.GetChild(0).transform.GetChild(3).gameObject;
        Turret.transform.rotation = Quaternion.AngleAxis(h, Turret.transform.up) * Turret.transform.rotation;
    }
    void move()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        transform.position += (v * transform.forward) * 6f * Time.deltaTime;
        this.transform.rotation = Quaternion.AngleAxis(h, this.transform.up) * this.transform.rotation;
    }
    Vector3 dir;
    bool isfirst;
    Transform cam;
    void camAction()
    {
        cam = Camera.main.transform;
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            isfirst = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            isfirst = false;
        }
        if (isfirst)
        {
            dir = this.transform.position + this.transform.up * 2.5f;
            //cam.rotation = this.transform.GetChild(0).GetChild(3).transform.rotation;
            cam.rotation = this.transform.Find("TankRenderers").Find("TankTurret").transform.rotation;
        }
        else
        {
            dir = this.transform.position + -this.transform.forward * 7 + this.transform.up * 3.5f;
            cam.rotation = this.transform.rotation;
        }
        cam.position = dir;
    }
    float a;
    void gimoa()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            AA.SetActive(true);

            a += 3f * Time.deltaTime;
            //AA.transform.up = transform.forward;
            
            AA.transform.position = transform.position + Turret.transform.forward * a * 5f;
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            fire(a);
            AA.transform.position = transform.position;
            AA.SetActive(false);
            a = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            fire(10f);
        }
    }
    //    if (Input.GetKeyDown(KeyCode.Space))
    //	{
    //        pressedFire = Time.time;
    //	}
    //if (Input.GetKeyUp(KeyCode.Space))
    //{
    //    float pw = Time.time - pressedFire;
    //    pw = Mathf.Clamp(pw, 5, 15);
    //}
    public void fire(float b)
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //float c = Mathf.Clamp(b, 5f, 10f);
        GameObject g = Instantiate(main.preshell);
        g.name = "bullet";
        //g.transform.forward = firePos.transform.forward;
        g.transform.forward = Turret.transform.forward;

        g.transform.position = firePos.position;
        g.transform.localScale = new Vector3(1, 1, 1) * 2f;
        g.AddComponent<Rigidbody>().AddForce(g.transform.forward * 5f * b,
                    ForceMode.VelocityChange);
        g.AddComponent<CapsuleCollider>();
        g.AddComponent<shell>();

        //}

    }
}
