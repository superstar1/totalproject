﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shell : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Plane")
        {
            GameObject g = Instantiate(Resources.Load<GameObject>("fx_fumefx_boom_01"), this.transform.position, Quaternion.identity);
            g.GetComponent<ParticleSystem>().loop = false;
            Destroy(g, 1);

        }
        else if (collision.gameObject.CompareTag("TankE"))
        {
            Destroy(collision.gameObject);
        }
        else
        {
            player._this.Damage(Random.Range(1f, 2f));
        }

        Destroy(gameObject);
        //GameObject g2 = Instantiate(Resources.Load<GameObject>("fx_fumefx_boom_01"), this.transform.position, Quaternion.identity);
        //g2.GetComponent<ParticleSystem>().loop = false;
        //Destroy(g2, 1);


    }
}
