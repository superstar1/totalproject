﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DivideCap : MonoBehaviour
{

    void Start()
    {
        orizinPos = GameObject.Find("Controller").GetComponent<RectTransform>().position;
        orizinPos1 = GameObject.Find("Controller1").GetComponent<RectTransform>().position;
        orizinPos2 = GameObject.Find("Controller2").GetComponent<RectTransform>().position;
        orizinPos3 = GameObject.Find("Controller3").GetComponent<RectTransform>().position;
        CorkOrizinPos = GameObject.Find("Cork").transform.position;
    }
    Transform CorkPos;
    Vector3 dir;
    // Update is called once per frame
    void Update()
    {
        CorkPos = GameObject.Find("Cork").GetComponent<Transform>();
        if (isPressed)
        {
            RectTransform rect = GameObject.Find("Controller").GetComponent<RectTransform>();
            rect.position = Input.mousePosition;

            dir = rect.position - new Vector3(orizinPos.x, 0, 0);
            CorkPos.transform.rotation =
            Quaternion.AngleAxis(dir.x / (Mathf.Abs(dir.x)) * 3f, CorkPos.transform.up) * CorkPos.transform.rotation;
            CorkPos.transform.position += new Vector3(0, 0.0005f, 0);
            if (CorkPos.position.y - CorkOrizinPos.y >= 0.3)
            {
                CorkPos.transform.parent = null;
                isPressed = false;
            }
        }
        else
        {
            GameObject.Find("Controller").GetComponent<RectTransform>().position =
            Vector3.Lerp(GameObject.Find("Controller").GetComponent<RectTransform>().position, orizinPos,
            3f * Time.deltaTime);
        }

        if (isPressed1)
        {
            RectTransform rect = GameObject.Find("Controller1").GetComponent<RectTransform>();
            rect.position =Input.mousePosition;

            dir = rect.position - new Vector3(orizinPos1.x, 0, 0);
            if (dir.x != 0 && dir.x < 0)
            {
                GameObject.Find("Plane").GetComponent<CurveShapeDeformer1>().Multiplier += 0.05f;
                if (GameObject.Find("Plane").GetComponent<CurveShapeDeformer1>().Multiplier > 15)
                {
                    GameObject.Find("Plane").transform.parent = null;
                    isPressed1 = false;
                }
            }
        }
        else
        {
            GameObject.Find("Controller1").GetComponent<RectTransform>().position =
            Vector3.Lerp(GameObject.Find("Controller1").GetComponent<RectTransform>().position, orizinPos1,
            3f * Time.deltaTime);
        }

        if (isPressed2)
        {
            RectTransform rect = GameObject.Find("Controller2").GetComponent<RectTransform>();
            rect.position = Input.mousePosition;

            dir = rect.position - new Vector3(0, orizinPos2.y, 0);
            if (dir.y != 0 && dir.y < 0)
            {
                Vector3 temp = GameObject.Find("can tall").transform.localScale;
                temp.y -= 0.001f;
                Debug.Log(temp);
                GameObject.Find("can tall").transform.localScale = temp;
                if (temp.y < 0.18f)
                {
                    GameObject.Find("can tall").SetActive(false);
                    isPressed2 = false;
                }
            }
        }
        else
        {
            GameObject.Find("Controller2").GetComponent<RectTransform>().position =
            Vector3.Lerp(GameObject.Find("Controller2").GetComponent<RectTransform>().position, orizinPos2,
            3f * Time.deltaTime);
        }

        if (isPressed3)
        {
            RectTransform rect = GameObject.Find("Controller3").GetComponent<RectTransform>();
            rect.position = Input.mousePosition;
            dir = rect.position - new Vector3(orizinPos1.x, 0, 0);
            if (GameObject.Find("Ripple").GetComponent<RippleDeformer>().PeakMultiplier <= 0)
            {
                isPressed3 = false;
                return;
            }
            if (dir.x != 0)
            {
                GameObject.Find("Ripple").GetComponent<RippleDeformer>().PeakMultiplier -= 0.007f;
            }
        }
            GameObject.Find("Controller3").GetComponent<RectTransform>().position =
Vector3.Lerp(GameObject.Find("Controller3").GetComponent<RectTransform>().position, orizinPos3,
3f * Time.deltaTime);
    }

    bool isPressed;
    bool isPressed1;
    bool isPressed2;
    bool isPressed3;
    Vector3 CorkOrizinPos;
    Vector3 orizinPos;
    Vector3 orizinPos1;
    Vector3 orizinPos2;
    Vector3 orizinPos3;
    public void CtrDown()
    {
        isPressed = true;
    }
    public void CtrUp()
    {
        isPressed = false;
    }
    public void CtrDown1()
    {
        isPressed1 = true;
    }
    public void CtrUp1()
    {
        isPressed1 = false;
    }
    public void CtrDown2()
    {
        isPressed2 = true;
    }
    public void CtrUp2()
    {
        isPressed2 = false;
    }
    public void CtrDown3()
    {
        isPressed3 = true;
    }
    public void CtrUp3()
    {
        isPressed3 = false;
    }
}
