﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFSM : MonoBehaviour
{
    GameObject target;
    Vector3 startPos, dis;
    void Start()
    {
        //빈 게임오브젝트 만들기
        GameObject firePos = new GameObject();
        //부모 오브젝트로 넣기
        firePos.transform.parent = this.transform.GetChild(0).GetChild(3);
        firePos.transform.localPosition = new Vector3(0, 0.4f, 1.2f);
        firePos.name = "firePos";

        target = GameObject.Find("Tank");
        dis = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
        th = Random.Range(3, 7);
        dir2 = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
        StartCoroutine(changDirection());
    }
    int th;
    // Update is called once per frame
    void Update()
    {
        Tracking();
    }
    Vector3 dir;
    Vector3 dir2;
    float distance;
    void Tracking()
    {
        if (target != null)
        {
            dir = target.transform.position - transform.position;
            distance = dir.magnitude;
            startPos = transform.position;
        }
        else
        {
            return;
        }
        if (distance < 5f)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 3f * Time.deltaTime);
            Attack();
        }
        if (distance >= 5f && distance < 10f)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 3f * Time.deltaTime);
            transform.position += dir.normalized * 3f * Time.deltaTime;
        }
        //if (distance >= 10f)
        //{
        //    float x = Time.time;
        //    transform.position = startPos + 5f * Mathf.Sin(x) * dis.normalized * Time.deltaTime;
        //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation((Mathf.Sin(x) * dis.normalized).normalized), 3f * Time.deltaTime);
        //}
        if (distance >= 10f)
        {
            transform.position += dir2 * Time.deltaTime;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir2), 3f * Time.deltaTime);
        }
    }
    IEnumerator changDirection()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            dir2 = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
            dir2.Normalize();
        }
    }

    float currentTime;
    void Attack()
    {
        currentTime += Time.deltaTime;
        if (currentTime > 3f)
        {
            GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            g.name = "Ebul";
            g.transform.position = transform.GetChild(1).position;
            g.transform.forward = dir;
            g.transform.localScale = new Vector3(1, 1, 1) * 0.3f;
            g.AddComponent<Rigidbody>().AddForce(g.transform.forward * 5f,
                ForceMode.VelocityChange);
            g.AddComponent<CapsuleCollider>();
            g.AddComponent<shell>();
            currentTime = 0;
            th = Random.Range(3, 7);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "bullet")
        {
            GameObject destroyEffect = Instantiate(Resources.Load<GameObject>("TankExplosion"),
                transform.position, Quaternion.identity);
            Debug.Log("hit");
            Destroy(gameObject);
        }
    }
}
