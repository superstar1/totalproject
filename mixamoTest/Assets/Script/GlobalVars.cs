﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVars
{
    public static float Player_Hp_MAX = 100;

    public static void ShellFire(Transform target, float pw)
    {
        GameObject g = MonoBehaviour.Instantiate(main.preshell);
        //g.transform.forward = firePos.transform.forward;
        g.transform.forward = target.transform.forward;

        g.transform.position = target.position;
        g.transform.localScale = new Vector3(1, 1, 1) * 0.3f;
        g.AddComponent<Rigidbody>().AddForce(target.forward * 5f * pw,
                    ForceMode.VelocityChange);
        g.AddComponent<CapsuleCollider>().isTrigger = true;
        g.AddComponent<shell>();

    }


}
