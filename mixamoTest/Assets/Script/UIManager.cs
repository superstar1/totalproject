﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;


public class UIManager : MonoBehaviour
{
    Vector3 orizinPos;
    GameObject target;
    private void Start()
    {
        orizinPos = GameObject.Find("Controller").GetComponent<RectTransform>().position;
        target = GameObject.Find("Tank");
    }
    Vector3 dir;
    float distance;
    Touch touches;
    private void Update()
    {
        if (isPressed)
        {
            RectTransform rect = GameObject.Find("Controller").GetComponent<RectTransform>();
            rect.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

            dir = rect.position - orizinPos;
            distance = dir.magnitude;
            if (distance > 20)
            {
                Vector3 temp = dir.normalized * 20;
                rect.position = temp + orizinPos;

                target.transform.position += this.transform.forward * (dir.y / (Mathf.Abs(dir.y))) * 3f * Time.deltaTime;
                //target.transform.rotation += this.transform.right * dir.x * Time.deltaTime;
                target.transform.rotation = Quaternion.AngleAxis(dir.x/(Mathf.Abs(dir.x)) * 3f, target.transform.up) * target.transform.rotation;
                // 부호 가지고 오기
                // 
            }
        }
        else
        {
            GameObject.Find("Controller").GetComponent<RectTransform>().position =
            Vector3.Lerp(GameObject.Find("Controller").GetComponent<RectTransform>().position, orizinPos,
               3f * Time.deltaTime);
        }

    }

    bool isPressed;
    public void btn1()
    {
        target.transform.position = Vector3.zero;
    }


    public void btn2()
    {
        GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        g.name = "bullet";

        //g.transform.forward = firePos.transform.forward;
        g.transform.forward = player._this.Turret.transform.forward;

        g.transform.position = player._this.transform.position + transform.forward * 5 + Vector3.up * 20;
        g.transform.localScale = new Vector3(1, 1, 1) * 10f;
        g.AddComponent<Rigidbody>().AddForce(g.transform.forward * 20f ,
                    ForceMode.VelocityChange);
        g.AddComponent<CapsuleCollider>();
        g.AddComponent<shell>();
    }


    public void CtrDown()
    {
        isPressed = true;
    }
    public void CtrUp()
    {
        isPressed = false;
    }
    public void booster()
    {
        player._this.fire(10f);
    }
}
