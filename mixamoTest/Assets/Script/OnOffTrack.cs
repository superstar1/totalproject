﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnOffTrack : MonoBehaviour
{
    GameObject Content;
    private void Start()
    {
        Content = GameObject.Find("contents");
        TrackOff();
    }
    public void TrackOn()
    {
        Content.SetActive(true);
    } 
    public void TrackOff()
    {
        Content.SetActive(false);
    }

}
