﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClosingLines : MonoBehaviour
{
    GameObject brPlate, crPlate;
    bool brIsTalking, crIsTalking;

    bool stopped, pickedup, afterc1, aftersurprised, metplayer;

    float currentTime;

    TypeWriterEffect typeWriterEffectbr = null;
    TypeWriterEffect typeWriterEffectcr = null;
    // Start is called before the first frame update
    void Start()
    {
        brPlate = GameObject.Find("brPlate");
        typeWriterEffectbr = GameObject.Find("brText").GetComponent<TypeWriterEffect>();
        brPlate.SetActive(false);

        crPlate = GameObject.Find("crPlate");
        typeWriterEffectcr = GameObject.Find("crText").GetComponent<TypeWriterEffect>();
        crPlate.SetActive(false);
    }

    public void brSays()
    {
        if (1f < currentTime && currentTime < 3f)
        {
            brPlate.SetActive(true);
            typeWriterEffectbr.Get_Typing("우와!");
        }
        else if (currentTime > 3 && currentTime < 7f)
        {
            brPlate.SetActive(false);
        }
        else if (6f < currentTime && currentTime < 10f) 
        {
            brPlate.SetActive(true);
            typeWriterEffectbr.Get_Typing("고마워! 네 덕분에 지구가 아름다운 자연을 되찾았어.");
        }
        else if (currentTime > 10.5f)
        {
            brPlate.SetActive(false);
        }

    }

    public void crSays()
    {
        if (3f < currentTime && currentTime < 6f)
        {
            crPlate.SetActive(true);
            typeWriterEffectcr.Get_Typing("이게 지구의 원래 모습이구나.");
        }
        if (7f < currentTime && currentTime < 9f)
        {
            crIsTalking = true;
            typeWriterEffectcr.Get_Typing("신나~!");
        }
        if (currentTime > 10 && currentTime < 11f)
        {
            crPlate.SetActive(false);
        }
        else if (11f < currentTime && currentTime < 16f)
        {
            crPlate.SetActive(true);
            typeWriterEffectcr.Get_Typing("네 도움이 필요한 친구들이 더 있어. 도와줄래?");
        }
        else if (currentTime > 16)
        {
            crPlate.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        brSays();
        crSays();
    }
}