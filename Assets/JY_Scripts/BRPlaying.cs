﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

public class BRPlaying : MonoBehaviour
{
    enum brPState
    {
        FirstIdle, Idle, Walk, Point, Stop, Talk
    }

    brPState br_PState;

    GameObject player, trash, brPlate;
    Text brText;
    Animator brPAnim;

    TypeWriterEffect typeWriterEffect = null;

    float cTime;
    Vector3 dir;
    bool waittoTalk;
    public float walkspeed = 1;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        trash = GameObject.Find("GlassBottle");
        brPlate = GameObject.Find("brPlate");
        typeWriterEffect = GameObject.Find("brText").GetComponent<TypeWriterEffect>();
        brPAnim = GetComponent<Animator>();
        print(brPAnim.runtimeAnimatorController.name);
        brPlate.SetActive(false);
        br_PState = brPState.FirstIdle;
        cTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        print(br_PState);
        switch (br_PState)
        {
            case brPState.FirstIdle:
                FirstIdle();
                break;
            case brPState.Idle:
                Idle();
                break;
            case brPState.Walk:
                Walk();
                break;
            case brPState.Point:
                Point();
                break;
            case brPState.Talk:
                Talk();
                break;
        }
    }

    private void FirstIdle()
    {
        cTime += Time.deltaTime;
        if (cTime < 1f)
        {
            Vector3 dir2 = player.transform.position - this.transform.position;
            dir2 = Vector3.ProjectOnPlane(dir2.normalized, Vector3.up);
            this.transform.forward = dir2;
        }
        else if (cTime > 1f && cTime < 4f)
        {
            brPAnim.SetBool("isPlayStart", true);
            dir = trash.transform.position - this.transform.position;
            dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir.normalized), 1.4f * Time.deltaTime);
        }
        else if (cTime > 4f)
        {
            brPAnim.SetBool("isWalking", false);
            print(brPAnim);
            br_PState = brPState.Walk;
            cTime = 0;
        }
    }

    private void Idle()
    {
        brPAnim.SetBool("isIdle", true);
        cTime += Time.deltaTime;
        if (cTime > 1.0f && cTime < 3.0f)
        {
            brPAnim.SetBool("isTurning", true);
        }
        if (cTime <= 1.0f || cTime >= 3.0f)
        {
            brPAnim.SetBool("isTurning", false);
        }
        if (cTime > 3.0f)
        {
            Vector3 dir2 = player.transform.position - this.transform.position;
            dir2 = Vector3.ProjectOnPlane(dir2.normalized, Vector3.up);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir2), 1.3f * Time.deltaTime);
            brPAnim.SetTrigger("walktrigger");
        }

        float plDist = Vector3.Distance(player.transform.position, this.transform.position);
        float pltrDist = Vector3.Distance(trash.transform.position, player.transform.position);

        if (plDist < 4.0f && waittoTalk == false)
        {
            dir = (trash.transform.position - this.transform.position).normalized;
            transform.forward = dir;
            brPAnim.SetTrigger("walktrigger");
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir), 1.4f * Time.deltaTime);
            br_PState = brPState.Walk;
            cTime = 0;
        }
        if (pltrDist < 6 && waittoTalk == true)
        {
            dir = (trash.transform.position - this.transform.position).normalized;
            transform.forward = dir;
            brPAnim.SetTrigger("walktrigger");
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir), 1.4f * Time.deltaTime);
            br_PState = brPState.Point;
            cTime = 0;
            brPAnim.SetBool("isIdle", false);
        }
    }


    private void Walk()
    {
        dir = (trash.transform.position - this.transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up);
        transform.forward = dir;
        brPAnim.SetBool("isWalking", true);
        transform.position += transform.forward * walkspeed * Time.deltaTime;
        float plDist = Vector3.Distance(player.transform.position, this.transform.position);
        if (plDist > 7.0f)
        {
            brPAnim.SetTrigger("Stops");
            br_PState = brPState.Idle;
            cTime = 0;
            brPAnim.SetBool("isWalking", false);
        }

        float trDist = Vector3.Distance(trash.transform.position, this.transform.position);
        float pltrDist = Vector3.Distance(trash.transform.position, player.transform.position);

        if (trDist < 6.0f)
        {
            if (pltrDist < 8.0f)
            {
                brPAnim.SetTrigger("Stops");
                br_PState = brPState.Point;
                cTime = 0;
                brPAnim.SetBool("isWalking", false);
            }
            else if (pltrDist > 9.0f)
            {
                brPAnim.SetTrigger("Stops");
                br_PState = brPState.Idle;
                cTime = 0;
                waittoTalk = true;
                brPAnim.SetBool("isWalking", false);
            }
        }
    }
    private void Point()
    {
        cTime += Time.deltaTime;
        print("State: Point");
        dir = (trash.transform.position - this.transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up);
        transform.forward = dir;
        brPAnim.SetTrigger("PointsinIdle");
        if (3.5f < cTime && cTime < 6)
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("앗! 저기에 다른 쓰레기가 있어.");

        }
        else if (6 < cTime)
        {
            brPlate.SetActive(false);
            br_PState = brPState.Talk;
            cTime = 0;
        }

    }
    private void Talk()
    {
        cTime += Time.deltaTime;
        print("State:Talk");
        brPAnim.SetBool("isTalking", true);
        if (0f < cTime && cTime < 2f)
        {
            brPAnim.SetBool("isTurning", true);
        }
        if (cTime > 2f)
        {
            brPAnim.SetBool("isTurning", false);
        }

        Vector3 dir = player.transform.position - this.transform.position;
        dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir.normalized), 1.0f * Time.deltaTime);
        if (2 < cTime && cTime < 5)
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("저건 어떻게 분리수거해야 해?");
        }
        if (5 < cTime)
        {
            brPlate.SetActive(false);
            brPAnim.SetBool("isIdle", true);
        }
    }
}
