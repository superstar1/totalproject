﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CROpening : MonoBehaviour
{
    GameObject player, canPos, br, can, crPlate;
    Text crText;
    Animator crAnim;
    float currentTime, dist;
    Vector3 dir;
    TypeWriterEffect typeWriterEffect = null;

    enum crState
    {
        Idle, Walk, Wonder, Surprised
    }
    crState cr_State;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        canPos = GameObject.Find("CanPos1");
        br = GameObject.Find("BR");

        crAnim = GetComponent<Animator>();
        crAnim.runtimeAnimatorController = Resources.Load("crAnim") as RuntimeAnimatorController;

        crPlate = GameObject.Find("crPlate");
        typeWriterEffect = GameObject.Find("crText").GetComponent<TypeWriterEffect>();
        crPlate.SetActive(false);

        cr_State = crState.Walk;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        crAnim.SetFloat("Time", currentTime);

        switch (cr_State)
        {
            case crState.Idle:
                Idle();
                break;
            case crState.Walk:
                Walk();
                break;
            case crState.Wonder:
                Wonder();
                break;
            case crState.Surprised:
                Surprised();
                break;
        }
    }

    private void Idle()
    {

    }
    private void Walk()
    {
        if (currentTime < 5)
        {
            dir = (canPos.transform.position - this.transform.position).normalized;
            dir = Vector3.ProjectOnPlane(dir, Vector3.up);
            transform.forward = dir;
        }
        else if(currentTime> 5)
        {
            cr_State = crState.Wonder;
        }
    }
    private void Wonder()
    {
        if (currentTime < 6) 
        {
            crAnim.SetBool("LegDelay", false);
        }
        else if (6 < currentTime && currentTime < 17) 
        {
            crAnim.SetBool("LegDelay", true);  
            if (6.3f < currentTime && currentTime < 7.3f)
            {
                Vector3 dir2 = br.transform.position - this.transform.position;
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir2), 3f * Time.deltaTime);
            }
            else if (8.5f < currentTime && currentTime < 10.5f)
            {
                crPlate.SetActive(true);
                typeWriterEffect.Get_Typing("우리 몸처럼 단단해 보인다.");
            }
            else if (currentTime > 10.5f && currentTime < 14f)
            {
                crPlate.SetActive(false);
            }

            else if(15< currentTime && currentTime < 17f)
            {
                crPlate.SetActive(true);
                typeWriterEffect.Get_Typing("무슨 로봇이 이렇게 작아?");
            }
            else if (currentTime > 17)
            {
                crPlate.SetActive(false);
            }
        }
        else if (17.5f < currentTime)
        {
            cr_State = crState.Surprised;
        }
    }
    private void Surprised()
    {
        crAnim.SetBool("LegDelay", false);

        if (17.5f < currentTime && currentTime < 18.5f) 
        {
            crPlate.SetActive(true);
            typeWriterEffect.Get_Typing("으악!");
        }
        if (currentTime > 19.5f) 
        {
            crPlate.SetActive(false);
        }
       
    }
        
}