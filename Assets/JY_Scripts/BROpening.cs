﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HighlightingSystem;

public class BROpening : MonoBehaviour
{
    GameObject player,can, brPlate;
    Transform canPos, canFloatPos;
    Animator brAnim;
    float currentTime, dist;
    Vector3 dir;
    TypeWriterEffect typeWriterEffect = null;

    enum brState
    {
        Idle, Walk, LiftandWonder, SurprisedandTalk
    }
    brState br_State;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        canPos = GameObject.Find("CanPos").transform;
        canFloatPos = GameObject.Find("CanFloatPos").transform;
        can = GameObject.Find("Can");
        can.SetActive(false);

        brAnim = GetComponent<Animator>();
        brAnim.runtimeAnimatorController = Resources.Load("brAnim") as RuntimeAnimatorController;

        brPlate = GameObject.Find("brPlate"); 
        typeWriterEffect = GameObject.Find("brText").GetComponent<TypeWriterEffect>();

        brPlate.SetActive(false);

        br_State = brState.Walk; 
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        brAnim.SetFloat("Time", currentTime);

        if (currentTime > 8.4f)
        {
            can.SetActive(true);
        }
        switch (br_State)
        {
            case brState.Idle:
                Idle();
                break;

            case brState.Walk:
                Walk();
                break;

            case brState.LiftandWonder:
                LiftandWonder();
                break;

            case brState.SurprisedandTalk:
                SurprisedandTalk();
                break;
        }
    }
   private void Idle()
    {

    }

    private void Walk()
    {
        if (currentTime < 5)
        {
            dir = (canPos.transform.position - this.transform.position).normalized;
            dir = Vector3.ProjectOnPlane(dir, Vector3.up);
            transform.forward = dir;
        }
        else if(5f < currentTime && currentTime < 6f)
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("어?  이게 뭐지?");
        }
        else if (currentTime>6 && currentTime<11)
        {
            brPlate.SetActive(false);
            br_State = brState.LiftandWonder;
        }
       
    }
    private void LiftandWonder()
    {
        if (12 < currentTime && currentTime < 15) 
        {
            brAnim.SetBool("LegDelay", true);
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("우리 같은 로봇의 부품일까?");
        }
        else if(16<currentTime && currentTime<24) 
        {
            brPlate.SetActive(false);
            br_State = brState.SurprisedandTalk;
        }
    }
    private void SurprisedandTalk()
    {
        
        if(17.5f<currentTime)
        {
            can.transform.parent = null;
            can.GetComponent<Highlighter>().enabled = true;
            Vector3 temp = can.transform.forward * Mathf.Sin(Time.time * 5);
            can.transform.position = Vector3.Lerp(can.transform.position, canFloatPos.position, 1f * Time.deltaTime) + temp*0.001f;
        }
        if (18.5f < currentTime && currentTime < 19.5f) 
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("깜짝이야!");
        }
        else if(19.5f< currentTime && currentTime<21f)
        {
            brPlate.SetActive(false);
        }
        else if(21f < currentTime && currentTime < 23f)
        {
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("이게 도대체 뭘까?");
        }
        else if(23f < currentTime && currentTime< 26)
        {
            brPlate.SetActive(false);
        }
        else if (28f < currentTime && currentTime < 31f)
        {
            brAnim.SetBool("PlayerInRange", true);
            brPlate.SetActive(true);
            typeWriterEffect.Get_Typing("안녕, 너 혹시 이게 뭔 줄 아니?");
        }
    }

    
}
