﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CRPlaying : MonoBehaviour
{
    enum crPState
    {
        CIdle, CWalk
    }

    crPState cr_PState;

    GameObject player, trash, CRpoint;
    //Text crText;
    Animator crPAnim;

    float cTime, velocity, dist;
    public float crSpeed;
    Vector3 dir;
    Transform playerPos;
    public bool isPMoving = true;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        trash = GameObject.Find("Trash");
        CRpoint = GameObject.Find("CRPoint");

        velocity = player.GetComponent<CharacterController>().velocity.magnitude;
        print(velocity);

        crPAnim = GetComponent<Animator>();
        crPAnim.runtimeAnimatorController = Resources.Load("crPAnim") as RuntimeAnimatorController;

        dir = (trash.transform.position - this.transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up);
        transform.forward = dir;

        crPAnim.SetBool("isCIdle", true);
        cr_PState = crPState.CIdle;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pInput = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        Vector2 sInput = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);

        float v = Input.GetAxis("Vertical");

        dist = Vector3.Distance(player.transform.position, this.transform.position);
        if (velocity == 0 && dist < 4)
        {
            isPMoving = false;
        }
        else
        {
            isPMoving = true;
        }

        switch (cr_PState)
        {
            case crPState.CIdle:
                CIdle();
                break;
            case crPState.CWalk:
                CWalk();
                break;
        }
    }

    private void CIdle()
    {
        crPAnim.SetBool("isCIdle", true);

        if (isPMoving == true)
        {
            cr_PState = crPState.CWalk;
            crPAnim.SetBool("isCIdle", false);
        }
    }

    private void CWalk()
    {
        if (isPMoving == true)
        {
            crPAnim.SetBool("isCWalking", true);

            Vector3 dir2 = (CRpoint.transform.position - this.transform.position).normalized;
            dir2 = Vector3.ProjectOnPlane(dir2, Vector3.up);
            this.transform.position += dir2 * crSpeed * Time.deltaTime;
        }
        else if (isPMoving == false)
        {
            crPAnim.SetTrigger("CStops");
            cr_PState = crPState.CIdle;
            crPAnim.SetBool("isCWalking", false);
        }
    }
}
