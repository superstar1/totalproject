﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalk : MonoBehaviour
{
    public float pWalkSpeed = 1;

    // Update is called once per frame
    void Update()
    {
        float v = Input.GetAxis("Vertical");
        this.transform.position += this.transform.forward * v * Time.deltaTime;
    }
}
