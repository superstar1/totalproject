﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRClosing : MonoBehaviour
{
    float currentTime;
    bool LegDelay3;
    GameObject cr, player;
    Animator crCAnim;

    public GameObject rbCManager;

    void Start()
    {
        cr = GameObject.Find("CR");
        player = GameObject.Find("Player");
        crCAnim = cr.GetComponent<Animator>();
        crCAnim.runtimeAnimatorController = Resources.Load("crCloAnim") as RuntimeAnimatorController;
   
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        crCAnim.SetFloat("Time", currentTime);
        if (currentTime > 4.3f && currentTime < 20)
        {
            Vector3 dir2 = player.transform.position - this.transform.position;
            dir2 = Vector3.ProjectOnPlane(dir2.normalized, Vector3.up);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir2), 1.3f * Time.deltaTime);
        }
        if (currentTime > 9.5f && currentTime < 20)
        {
            crCAnim.SetBool("LegDelay3", true);
        }
    }
}
