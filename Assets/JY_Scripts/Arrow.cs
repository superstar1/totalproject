﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arrow : MonoBehaviour
{
    GameObject arrow;
    Transform player, point;
    float cTime, dist;
    bool saidYes;
    GameObject choosecanvas = null;
    startUI startUI = null;
    closeUI closeUI = null;
    // Start is called before the first frame update
    void Start()
    {
        arrow = GameObject.Find("Arrow");
        arrow.gameObject.SetActive(false);
        player = GameObject.Find("Player").transform;
        point = GameObject.Find("WayPoint").transform;
        choosecanvas = GameObject.Find("ChooseCanvas");
        startUI = choosecanvas.GetComponent<startUI>();
    }

    // Update is called once per frame
    void Update()
    {
        cTime += Time.deltaTime;
        if (!startUI.instance.isMax) 
        {
            Vector3 dir = (point.position - player.transform.position).normalized;
            dir = Vector3.ProjectOnPlane(dir.normalized, Vector3.up);
            arrow.transform.forward = dir;
            arrow.transform.position = player.position + -player.right * 0.4f + -player.up * 0f + player.forward * 7.0f; 
            arrow.gameObject.SetActive(true);
        }
    }
}