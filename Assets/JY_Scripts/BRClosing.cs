﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BRClosing : MonoBehaviour
{
    float currentTime;
    bool LegDelay2;
    GameObject br;
    Animator brCAnim;

    // Start is called before the first frame update
    void Start()
    {
        br = GameObject.Find("BR");
        brCAnim = br.GetComponent<Animator>();
        brCAnim.runtimeAnimatorController = Resources.Load("brCloAnim") as RuntimeAnimatorController;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        brCAnim.SetFloat("Time", currentTime);
        if(currentTime>3.9f && currentTime< 15)
        {
            brCAnim.SetBool("LegDelay2", true);
        }
        else
        {
            brCAnim.SetBool("LegDelay2", false);
        }
    }
}
