﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startUI : MonoBehaviour
{
    public GameObject ex;
    //다가오면 설명이 뜬다 
    //다가왔을 때 설명이 점점 1.5초동안 원래 크기까지 커진다 
    //빠져나가면 설명이 점점 1초동안 0으로 작아진다 
    //필요속성 : 경과시간, 변화시간, 원래 크기
    float currentTime;
    float changeTime = 0.5f;
    Vector3 originSize;
    //설명이 플레이어를 계속 바라보게 하고싶다.
    //필요속성 : 플레이어
    public Transform playerTransform;
    public static startUI instance;
    public bool clickedYes;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {

        originSize = ex.transform.localScale;

    }

    private void Update()
    {

        starting();

        print(isMax);

    }

    public bool isMax = true;
    public void starting()
    {

        currentTime += Time.deltaTime;
        if (currentTime < changeTime && isMax)
        {
            print("11");
            ex.transform.localScale = Vector3.Lerp(Vector3.zero, originSize, currentTime / changeTime);

        }
        else if (currentTime < changeTime && !isMax)
        {
            print("22");
            //ex.transform.localScale = originSize;
            ex.transform.localScale = Vector3.Lerp(originSize, Vector3.zero, currentTime / changeTime);
                if (currentTime >= changeTime)
                {
                    ex.SetActive(false);
                }//currentTime = 0;*/
        }

    }

    public void closing()
    {
        isMax = false;
        clickedYes = true;
        currentTime = 0;
    }


}


