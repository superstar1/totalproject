﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closeUI : MonoBehaviour
{
    public bool isClosed = false;
    IEnumerator closedUI(GameObject ex)
    {

        float currentTime = 0;
        float changeTime= 0.5f;
        Vector3 originSize = ex.transform.localScale;

        /*while (true)
        {*/
            isClosed = false; 
            yield return new WaitForSeconds(Time.deltaTime);

            currentTime += Time.deltaTime;
            if (currentTime < changeTime)
            {
                ex.transform.localScale = Vector3.Lerp(originSize, Vector3.zero, currentTime / changeTime);

            }
            else
            {
                ex.transform.localScale = Vector3.zero;
                //Destroy(ex);
                ex.SetActive(false);
                isClosed = true; 
               // break;
            }
       /* }*/
    }


    // Start is called before the first frame update
   public void closedUITrigger(GameObject ex)
    {
        StartCoroutine(closedUI(ex));
    }
}
